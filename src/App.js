import React from 'react';
import logo from './logo.svg';
import './App.css';
import Blockly from 'node-blockly/browser'; 
 
import BlocklyDrawer, { Block, Category } from 'react-blockly-drawer';

/* const helloWorld =  {
  name: 'Echobit',
  category: 'Echobit',
  block: {
    init: function () {
      this.jsonInit({
        type: "controls_repeat_ext",
        message0: "Listen every %1 seconds",
        args0: [
          {
            type: "field_input",
            name: "NAME",
            text: "1"
          }
        ],
        message1: "do %1",
        args1: [
          {"type": "input_statement", "name": "DO"}
        ],
        previousStatement: null,
        nextStatement: null,
        colour: 120
      });
    },
  },
  generator: (block) => {
    const message = `'${block.getFieldValue('NAME')}'` || '\'\'';
    const {childBlocks_} = block;
    let internal; 
    if (childBlocks_.length > 0 && childBlocks_[0].type === 'alexaSay') {
      internal = "alexaApi('say', 'Estas recibiendo una señal')"
    }
    const code = `setInterval(() => {${internal ? internal : ''}}, ${message})`;
    return code;
  },
}; */

const alexaSay =  {
  name: 'alexaSay',
  category: 'Echobit',
  block: {
    init: function () {
      this.jsonInit({
        "message0": "Alexa Say %1",
        "args0": [
          {"type": "input_value", "name": "DELTA", "check": "Number"}
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230
      });
    },
  },
  generator: (block) => {
    const message = `'${block.getFieldValue('DELTA')}'` || '\'\'';
    const code = `callAlexa()`;
    return code;
  },
};

const alexaShow =  {
  name: 'alexaShow',
  category: 'Echobit',
  block: {
    init: function () {
      this.jsonInit({
        "message0": "Alexa Show %1",
        "args0": [
          {"type": "input_value", "name": "DELTA", "check": "Number"}
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 230
      });
    },
  },
  generator: (block) => {
    const message = `'${block.getFieldValue('NAME')}'` || '\'\'';
    const code = `console.log('Hello ${message}')`;
    return [code, Blockly.JavaScript.ORDER_MEMBER];
  },
};

const sayLabel =  {
  name: 'sayLabel',
  category: 'Echobit',
  block: {
    init: function () {
      this.jsonInit({
        "message0": "%1",
        "args0": [
          {
            type: "field_input",
            name: "TEXT",
            text: "Estas recibiendo una señal"
          }
        ],
         "output": null,
        "colour": 230
      });
    },
  },
  generator: (block) => {
    const message = `'${block.getFieldValue('NAME')}'` || '\'\'';
    const code = `console.log('Hello ${message}')`;
    return [code, Blockly.JavaScript.ORDER_MEMBER];
  },
};


class App extends React.Component {

  state = {
    code: '',
  }

  helloWorld =  {
    name: 'Echobit',
    category: 'Echobit',
    block: {
      init: function () {
        this.jsonInit({
          type: "controls_repeat_ext",
          message0: "Listen every %1 seconds",
          args0: [
            {
              type: "field_input",
              name: "NAME",
              text: "1"
            }
          ],
          message1: "do %1",
          args1: [
            {"type": "input_statement", "name": "DO"}
          ],
          previousStatement: null,
          nextStatement: null,
          colour: 120
        });
      },
    },
    generator: (block) => {
      const message = `'${block.getFieldValue('NAME')}'` || '\'\'';
      const {childBlocks_} = block;
      let internal; 
      if (childBlocks_.length > 0 && childBlocks_[0].type === 'alexaSay') {
        this.setState({code: "alexaApi('say', 'Estas recibiendo una señal')"});
      }
      const code = `setInterval(() => {${internal ? internal : ''}}, ${message})`;
      return code;
    },
  };


  render() {
    return (
      <div className="App">
        <div style={{display: 'flex',alignContent: 'flex-start'}}>
          <img style={{margin: '10px'}} height="70px" src={require("./echobit.jpg")} />
        </div>
        <BlocklyDrawer
          tools={[this.helloWorld, alexaSay, alexaShow, sayLabel]}
          onChange={(code) => {
            
          }}
          appearance={
            {
              categories: {
                Demo: {
                  colour: '270'
                },
              },
            }
          }
        >
          <Category name="Variables" custom="VARIABLE" />
          {/* <Category name="Values">
            <Block type="math_number" />
            <Block type="text" />
          </Category> */}
        </BlocklyDrawer>
      </div>
    )
  }
}

export default App;
